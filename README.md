# UnicodeInput ReadMe

Simple HTML to search for completion sequences for `Julia REPL` and `AsciiMath`.

# Dependencies

- [Julia](https://julialang.org/).
- [Node.js](https://nodejs.org/).

## Build Instruction

```sh
julia input.data.jl
```

## Usage

```sh
<path-to-browser> index.html
```

> [!IMPORTANT]
> AsciiMath only contains similar completion sequences as `Julia REPL`.

## Change Log

- __v0.0.1:__ Initial working viewer for `Julia REPL` and `AsciiMath` completion sequences.
- __v0.0.2:__ Refactor and search fixes.
- __v0.0.3:__ Use `DelayedAction` to fix typing latency.
- __v0.0.4:__ Use shadow DOM to speedup search rendering.
- __v0.0.5:__ Relocate asserts.