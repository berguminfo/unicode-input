// unicode-input. Copyright © 2023 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

const fs = require("fs");
const path = require("path");

// set global mocks
const nodeMock = {
    type: "",
    appendChild: function() {}
};
global.navigator = {
    appName: ""
};
global.document = {
    getElementById: function() {},
    getElementsByTagName: function() { return [nodeMock]; },
    createElement: function() { return nodeMock; },
    createTextNode: function() { return nodeMock; }
};
global.window = {
    addEventListener: undefined
};

// load global.asciimath
// NOTE: ensure to export the AMsymbols object
const source = fs.readFileSync(path.join(path.dirname(__dirname), "lib", "ASCIIMathML.js"));
eval(source.toString());
if (!asciimath.AMsymbols) {
    console.error("Error: missing symbol 'AMsymbols'");
    process.exit(1);
}

// generate json
const result = {};
for (const element of asciimath.AMsymbols) {
    if (typeof element.output === "string" && element.output.length == 1) {
        const key = element.output.charCodeAt(0);
        result[`U+${key.toString(16).padStart(5, 0).toUpperCase()}`] = element.input;
    }
}

console.log(JSON.stringify(result, null, "  "));
