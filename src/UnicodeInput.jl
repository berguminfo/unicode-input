# copy of unicode-inline.md having custom table_entries function

module UnicodeInput

using JSON
include("../lib/unicode-input.jl")

function table_entries(completions, unicode_dict, amsymbol_dict)
    entries = []
    for (chars, inputs) in sort!(collect(completions), by = first)
        code_points, unicode_names, characters, am_inputs = String[], String[], String[], String[]
        for char in chars
            code = "U+$(uppercase(string(UInt32(char), base = 16, pad = 5)))"
            amsymbol = length(chars) == 1 ? get(amsymbol_dict, code, nothing) : nothing
            push!(code_points, code)
            push!(unicode_names, get(unicode_dict, UInt32(char), "(No Unicode name)"))
            push!(characters, isempty(characters) ? fix_combining_chars(char) : "$char")
            if !isnothing(amsymbol)
                push!(am_inputs, amsymbol)
            end
        end
        push!(entries, [
            join(code_points, " + "),
            join(characters),
            join(inputs, ", "),
            join(am_inputs, ", "),
            join(unicode_names, " + ")
        ])
    end
    entries
end

function amsymols_dict()
    file = normpath(@__DIR__, "AMsymbols.js")
    source = read(`node $file`, String)
    JSON.parse(source)
end

function save_json(table)
    file = normpath(@__DIR__, "../wwwroot/assets/index.data.js")
    open(file, "w") do io
        write(io, "const UnicodeInputData=")
        write(io, JSON.json(table))
    end
    println("Saved to '$(file)'")
end

function run()
    table = table_entries(
        tab_completions(
            REPL.REPLCompletions.latex_symbols,
            REPL.REPLCompletions.emoji_symbols
        ),
        unicode_data(),
        amsymols_dict()
    )
    save_json(table)
    nothing
end

end # module