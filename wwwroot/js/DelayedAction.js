// DelayedAction. Copyright © 2023 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

/**
  * Represents a replacement to setTimeout using the requestAnimationFrame API.
*/
class DelayedAction {
    running = false;

    /**
     * Initializes a new instance of this class.
     *
     * @param {Function} action The action to call once the delay has been completed.
     * @param {number} delay The number of milliseconds to delay the action call.
     */
    constructor(action, delay) {
        this.action = action;
        this.delay = delay;
        this.handleAnimationFrame = this.handleAnimationFrame.bind(this);
    }

    /**
     * Clears the timer.
     */
    clear() {
        this.running = false;
    }

    /**
     * Resets the timer.
     */
    reset() {
        this.expires = Date.now() + this.delay;
        if (!this.running && typeof requestAnimationFrame === "function") {
            this.running = true;
            requestAnimationFrame(this.handleAnimationFrame);
        }
    }

    /**
     * Handler for requestAnimationFrame.
     */
    handleAnimationFrame() {
        if (this.running) {
            if (Date.now() > this.expires) {
                this.running = false;
                this.action();
            } else {
                requestAnimationFrame(this.handleAnimationFrame);
            }
        }
    }
}
