// unicode-input. Copyright © 2023 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

interface DelayedAction {
    running: boolean;
    action: () => void;
    delay: number;
    start: number;

    constructor(action: () => void, delay: number);

    clear();
    reset();
    handleAnimationFrame();
}

interface SearchOptions {
    query: string;
    require: "" | "AM"
    index: number;
    take: number;
}

type MatchHandler = (record: string[]) => void;

interface SearchResult {
    continuationIndex: number;
    empty: boolean;
    complete: boolean;
}

export function searchInput(input: string[][], options: SearchOptions, matchHandler: MatchHandler): SearchResult;
