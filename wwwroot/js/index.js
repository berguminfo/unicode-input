// unicode-input. Copyright © 2023 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

// Some common .aside class names.
const SHOW = 1;
const HIDING = 2;

/**
 * Sets the `.aside` element `show` or `hiding` attribute.
 *
 * @param {number} apply
 */
function aside(apply) {
    const asideElement = document.getElementById("aside");
    asideElement.classList.remove(apply == SHOW ? "hiding" : "show");
    asideElement.classList.add(apply == SHOW ? "show" : "hiding");
    const copyButton = document.getElementById("copy-button");
    if (apply == SHOW) {
        copyButton.removeAttribute("disabled");
    } else {
        copyButton.setAttribute("disabled", "");
    }
}

/**
 * Populate and show the `aside` element.
 *
 * @param {MouseEvent} event
 */
function rowClicked(event) {
    const target = event.target.closest("div[onclick]");
    if (!target) {
        console.warn("Closest .row not found", event);
        return;
    }

    // Accept Enter keyup event.
    if (event.key && event.code !== "Enter") {
        return;
    }

    // Populate the aside elements.
    for (let i = 0; ; ++i) {
        if (!target.children[i]) break;
        const aside = document.getElementById(`aside-${i}`);
        if (aside) aside.innerText = target.children[i].innerText;
    }

    // Let sidebar come into view.
    aside(SHOW);
}

/**
 * Add the `load` event handler.
 */
addEventListener("load", () => {
    const searchComponent = document.getElementById("search-component");

    // Escape should hide the sidebar.
    document.documentElement.addEventListener("keyup", event => {
        if (event.code === "Escape") {
            aside(HIDING);
        }

        // Dispatch accesskey events.
        if (event.altKey) {
            for (const element of document.querySelectorAll("button[accesskey]")) {
                if (event.code == `Key${element.getAttribute("accesskey").toUpperCase()}`) {
                    element.dispatchEvent(new MouseEvent("click"));
                }
            }
        }
    });

    // Changed search result should update header and footer.
    searchComponent.addEventListener("change", event => {
        document.getElementById("header").classList.toggle("d-none", event.detail.empty);
        document.getElementById("footer").classList.toggle("d-none", event.detail.complete);
    });

    // Adding characters to search query should update the `query` attribute.
    document.getElementById("search-input").addEventListener("keyup", event => {
        event.cancelBubble = true;
        searchComponent.setAttribute("query", event.target.value);
        aside(HIDING);
    });

    // Changing requirement should update the `require` attribute.
    document.getElementById("search-unicode").addEventListener("click", event => {
        searchComponent.setAttribute("require", event.target.value);
        aside(HIDING);
    });
    document.getElementById("search-am").addEventListener("click", event => {
        searchComponent.setAttribute("require", event.target.value);
        aside(HIDING);
    });

    // Clicking the `More` button should append more matching the attributes.
    document.getElementById("more-button").addEventListener("click", () =>
        searchComponent.search(NaN, 50));

    // Clicking the `All` button should start a new search taking all matching the attributes.
    document.getElementById("all-button").addEventListener("click", () =>
        searchComponent.search(0, Infinity));

    // Clicking the 'Copy and Hide' button should copy the character(s) to clipboard.
    document.getElementById("copy-button").addEventListener("click", event => {
        if (event.target.getAttribute("disabled") == null) {
            const text = document.getElementById("aside-1").innerText;
            navigator.clipboard.writeText(text);
            console.debug("Copy", text);
            aside(HIDING);
        }
    });

    // Clicking the `Close` button should dismiss the sidebar.
    document.getElementById("close-button").addEventListener("click", () =>
        aside(HIDING));
});
