// unicode-input. Copyright © 2023 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

/**
 * Represents the `search-component` HTMLElement.
 */
customElements.define("search-component", class extends HTMLElement {

    /**
     * The continuation index to use if no index provided to `search()`.
     */
    _continuationIndex = 0;

    /**
     * https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements#using_the_lifecycle_callbacks
     */
    static get observedAttributes() {
        return ["query", "require"];
    }

    /**
     * Initializes a new instance of this class.
     */
    constructor() {
        super();

        this.search = this.search.bind(this);

        this.input = UnicodeInputData;
        this.template = this.children[0];
        this.delayedAction = new DelayedAction(this.search, 250);

        this.attachShadow({ mode: "open" });
        this.shadowRoot.append(document.querySelector("link[rel=stylesheet][href]").cloneNode());

        this.mainElement = document.createElement("main");
        this.shadowRoot.append(this.mainElement);
    }

    /**
     * https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements#using_the_lifecycle_callbacks
     *
     * @param {string} name
     * @param {string} oldValue
     * @param {string} newValue
     */
    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue !== newValue) {
            //console.debug("attributeChanged", name, oldValue, newValue);
            if (name === "query") {
                this.delayedAction.reset();
            } else {
                this.search();
            }
        }
    }

    /**
     * Invoke search on `UnicodeInputData` and populate the shadow DOM.
     *
     * @param {number} index The search starting index.
     * @param {number} take The number of records to match at most.
     */
    search(index = 0, take = 10) {

        // clear on new search
        if (index == 0) this.mainElement.innerHTML = "";

        // start search and display the matching records
        const options = {
            query: this.getAttribute("query") || "",
            require: this.getAttribute("require") || "",
            index: !isNaN(index) ? index : this._continuationIndex + 1,
            take: take
        };
        console.debug("search", options);

        const result = searchInput(this.input, options, match => {
            const row = this.template.cloneNode(true);
            for (let i = 0; i < row.children.length; ++i) {
                row.children[i].innerText = match[i] || "";
            }
            this.mainElement.append(row);
        });

        this._continuationIndex = result.continuationIndex;

        // notify listeners
        this.dispatchEvent(new CustomEvent("change", { bubbles: true, detail: result }));
    }
});
