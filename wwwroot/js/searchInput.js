// unicode-input. Copyright © 2023 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.

const AM_INDEX = 3;

/**
 * Generic function to search for records given input and query.
 *
 * @param {string[][]} input
 * @param {object} options
 * @param {string[] => void} matchHandler
 * @returns {
 *  continuationIndex: number,
 *  empty: boolean,
 *  complete: boolean
 * }
 */
function searchInput(input, options, matchHandler) {
    options.query = options.query.toUpperCase();
    const result = {
        continuationIndex: 0,
        empty: true,
        complete: true
    };

    // Start search at incoming index but stop at end of data.
    let count = 0;
    for (let i = options.index; i < input.length; ++i) {
        const record = input[i];

        // check AsciiMath requirements
        if (options.require === "AM" && record[AM_INDEX].length == 0) {
            continue;
        }

        // Preserve last matching index for more.
        result.continuationIndex = i;

        // Test all text items.
        const match = record.find(element => element.toUpperCase().indexOf(options.query) >= 0);
        if (match) {

            // Display the matching record.
            matchHandler(record);

            // Stop search when reaching take.
            if (++count >= options.take) {
                break;
            }
        }
    }

    // Update result (should add .d-hide class if true).
    result.empty = count == 0;
    result.complete = count < options.take;

    return result;
}
